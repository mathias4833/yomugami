use actix_web::body::BoxBody;
use actix_web::http::StatusCode;
use actix_web::{HttpResponse, ResponseError};
use std::fmt;

#[derive(Debug)]
pub enum YomugamiError {
    ConnectorFailure(String),
    ParseError(String),
    BadRequest(String),
    MangaUnavailable(String),
}

impl fmt::Display for YomugamiError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            YomugamiError::ConnectorFailure(error) => write!(f, "Connector failure: {}", error),
            YomugamiError::ParseError(error) => write!(f, "Parse error: {}", error),
            YomugamiError::BadRequest(error) => write!(f, "Bad request: {}", error),
            YomugamiError::MangaUnavailable(error) => write!(f, "Manga unavailable: {}", error),
        }
    }
}

impl std::error::Error for YomugamiError {}

impl ResponseError for YomugamiError {
    fn status_code(&self) -> StatusCode {
        match self {
            YomugamiError::ConnectorFailure(_) => StatusCode::INTERNAL_SERVER_ERROR,
            YomugamiError::ParseError(_) => StatusCode::INTERNAL_SERVER_ERROR,
            YomugamiError::BadRequest(_) => StatusCode::BAD_REQUEST,
            YomugamiError::MangaUnavailable(_) => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }

    fn error_response(&self) -> HttpResponse<BoxBody> {
        HttpResponse::build(self.status_code()).json(self.to_string())
    }
}

pub type YomugamiResult<T> = Result<T, YomugamiError>;
