use crate::connectors::{ConnectorType, ConnectorsState};
use crate::error::YomugamiResult;
use crate::models::{Chapter, Image, Manga, MangaPreviewList};
use actix_web::web::Data;
use actix_web::{get, web, HttpResponse};
use strum::VariantNames;

#[get("/connectors")]
pub async fn connectors() -> HttpResponse {
    HttpResponse::Ok().json(ConnectorType::VARIANTS)
}

#[get("/{connector_type}/search/{manga_title}")]
pub async fn search_manga(
    state: Data<ConnectorsState>,
    path: web::Path<(ConnectorType, String)>,
) -> YomugamiResult<MangaPreviewList> {
    let (connector_type, manga_title) = path.into_inner();
    state
        .get_connector(connector_type)
        .search_manga(&manga_title)
        .await
}

#[get("/{connector_type}/manga/{manga_id}")]
pub async fn fetch_manga(
    state: Data<ConnectorsState>,
    path: web::Path<(ConnectorType, String)>,
) -> YomugamiResult<Manga> {
    let (connector_type, manga_id) = path.into_inner();
    state
        .get_connector(connector_type)
        .fetch_manga(&manga_id)
        .await
}

#[get("/{connector_type}/chapter/{chapter_id}")]
pub async fn fetch_chapter(
    state: Data<ConnectorsState>,
    path: web::Path<(ConnectorType, String)>,
) -> YomugamiResult<Chapter> {
    let (connector_type, chapter_id) = path.into_inner();
    state
        .get_connector(connector_type)
        .fetch_chapter(&chapter_id)
        .await
}

#[get("/{connector_type}/image/{image_id}")]
pub async fn fetch_image(
    state: Data<ConnectorsState>,
    path: web::Path<(ConnectorType, String)>,
) -> YomugamiResult<Image> {
    let (connector_type, image_id) = path.into_inner();
    state
        .get_connector(connector_type)
        .fetch_image(image_id)
        .await
}
