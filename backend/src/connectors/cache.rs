use reqwest::Url;
use std::collections::HashMap;
use std::sync::Arc;
use std::time::Duration;
use tokio::sync::Mutex;
use tokio::time;
use tokio::time::Instant;
use uuid::Uuid;

struct ExpiringValue {
    value: Url,
    expiration_date: Instant,
}

pub struct UrlCache {
    uuid_to_url: Mutex<HashMap<String, ExpiringValue>>,
    url_to_uuid: Mutex<HashMap<Url, String>>,
    expiration_duration: Duration,
}

impl UrlCache {
    pub fn new(expiration_duration: Duration, interval: Duration) -> Arc<Self> {
        let cache = Arc::new(UrlCache {
            uuid_to_url: Mutex::new(HashMap::new()),
            url_to_uuid: Mutex::new(HashMap::new()),
            expiration_duration,
        });

        let interval_task = {
            let cache_clone = cache.clone();
            tokio::spawn(async move {
                let mut interval = time::interval(interval);
                loop {
                    interval.tick().await;
                    cache_clone.remove_expired().await;
                }
            })
        };
        tokio::spawn(async move { interval_task.await.expect("Expiration task panicked") });

        cache
    }

    async fn insert(&self, uuid: String, url: Url) {
        let expiring = ExpiringValue {
            value: url.clone(),
            expiration_date: Instant::now() + self.expiration_duration,
        };
        self.uuid_to_url.lock().await.insert(uuid.clone(), expiring);
        self.url_to_uuid.lock().await.insert(url, uuid.clone());
    }

    async fn remove_expired(&self) {
        let now = Instant::now();

        let mut uuid_to_url_mutex = self.uuid_to_url.lock().await;
        let mut url_to_uuid_mutex = self.url_to_uuid.lock().await;

        uuid_to_url_mutex.retain(
            |_,
             ExpiringValue {
                 expiration_date, ..
             }| *expiration_date > now,
        );
        url_to_uuid_mutex.retain(|_, uuid| uuid_to_url_mutex.get(uuid).is_some());
    }

    pub async fn get_uuid(&self, url: &Url) -> Option<String> {
        self.url_to_uuid.lock().await.get(url).cloned()
    }

    pub async fn get_url(&self, uuid: String) -> Option<Url> {
        self.uuid_to_url
            .lock()
            .await
            .get(&uuid)
            .and_then(|expiring| Some(&expiring.value))
            .cloned()
    }

    pub async fn add_url(&self, url: Url) -> String {
        if let Some(uuid) = self.get_uuid(&url).await {
            if let Some(expiring) = self.uuid_to_url.lock().await.get_mut(&uuid) {
                expiring.expiration_date = Instant::now() + self.expiration_duration;
            }
            uuid.to_string()
        } else {
            let uuid = Uuid::new_v4().to_string();
            self.insert(uuid.clone(), url).await;
            uuid
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::connectors::cache::UrlCache;
    use reqwest::Url;
    use std::time::Duration;

    #[tokio::test(flavor = "multi_thread")]
    async fn test_url_cache() {
        let expiration_duration = Duration::from_millis(500);
        let interval = Duration::from_millis(100);
        let cache = UrlCache::new(expiration_duration, interval);

        let url1 = Url::parse("https://example.com/image1.png").unwrap();
        let url2 = Url::parse("https://example.com/image2.png").unwrap();
        let uuid1 = cache.add_url(url1.clone()).await;
        let uuid2 = cache.add_url(url2.clone()).await;

        assert_ne!(uuid1, uuid2);

        let retrieved_url1 = cache.get_url(uuid1.clone()).await;
        let retrieved_url2 = cache.get_url(uuid2.clone()).await;

        assert_eq!(retrieved_url1, Some(url1));
        assert_eq!(retrieved_url2, Some(url2));

        std::thread::sleep(Duration::from_millis(700));

        let expired_url1 = cache.get_url(uuid1).await;
        let expired_url2 = cache.get_url(uuid2).await;

        assert!(expired_url1.is_none());
        assert!(expired_url2.is_none());

        let url3 = Url::parse("https://example.com/image3.png").unwrap();
        let uuid3 = cache.add_url(url3.clone()).await;
        let retrieved_url3 = cache.get_url(uuid3.clone()).await;

        assert_eq!(retrieved_url3, Some(url3.clone()));

        std::thread::sleep(Duration::from_millis(200));

        let retrieved_url3_after_200_millis = cache.get_url(uuid3.clone()).await;

        assert_eq!(retrieved_url3_after_200_millis, Some(url3));

        std::thread::sleep(Duration::from_millis(700));

        let expired_url3 = cache.get_url(uuid3).await;

        assert!(expired_url3.is_none());
    }
}
