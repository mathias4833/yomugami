use crate::connectors::mangadex::MangaDexConnector;
use crate::connectors::mangakakalot::MangaKakalotConnector;
use crate::connectors::mangarockteam::MangaRockTeamConnector;
use crate::models::Connector;
use serde::{Deserialize, Serialize};
use std::sync::Arc;
use strum::{EnumString, EnumVariantNames};

mod cache;
mod mangadex;
mod mangakakalot;
mod mangarockteam;
mod throttled_client;
mod util;

#[derive(Deserialize, Serialize, EnumString, EnumVariantNames)]
#[serde(rename_all = "lowercase")]
#[strum(serialize_all = "lowercase")]
pub enum ConnectorType {
    MangaDex,
    MangaKakalot,
    MangaRockTeam,
}

pub struct ConnectorsState {
    mangadex: Arc<Box<dyn Connector + Send + Sync + 'static>>,
    mangakakalot: Arc<Box<dyn Connector + Send + Sync + 'static>>,
    mangarockteam: Arc<Box<dyn Connector + Send + Sync + 'static>>,
}

impl ConnectorsState {
    pub fn new() -> Self {
        ConnectorsState {
            mangadex: Arc::new(Box::new(MangaDexConnector::new())),
            mangakakalot: Arc::new(Box::new(MangaKakalotConnector::new())),
            mangarockteam: Arc::new(Box::new(MangaRockTeamConnector::new())),
        }
    }

    pub fn get_connector(
        &self,
        connector_type: ConnectorType,
    ) -> Arc<Box<dyn Connector + Send + Sync + 'static>> {
        match connector_type {
            ConnectorType::MangaDex => self.mangadex.clone(),
            ConnectorType::MangaKakalot => self.mangakakalot.clone(),
            ConnectorType::MangaRockTeam => self.mangarockteam.clone(),
        }
    }
}
