use async_trait::async_trait;
use chrono::TimeZone;
use futures::future;
use reqwest::header::{HeaderMap, HeaderValue, REFERER};
use reqwest::Url;
use scraper::{Element, ElementRef, Selector};
use tokio::time::Duration;

use crate::connectors::throttled_client::ThrottledClient;
use crate::connectors::util;
use crate::error::YomugamiError::{BadRequest, ParseError};
use crate::error::YomugamiResult;
use crate::models::{
    Chapter, ChapterPreview, Connector, Image, Manga, MangaPreview, MangaPreviewList,
};

pub struct MangaKakalotConnector {
    client: ThrottledClient,
    search_url: Url,
    manga_url: Url,
    manga_alias_url: Url,
    search_manga_selector: Selector,
    cover_selector: Selector,
    desc_selector: Selector,
    status_selector: Selector,
    chap_title_selector: Selector,
    chap_date_selector: Selector,
    image_selector: Selector,
}

impl MangaKakalotConnector {
    pub fn new() -> Self {
        let mut headers = HeaderMap::new();
        headers.insert(
            REFERER,
            HeaderValue::from_static("https://chapmanganato.to/"),
        );

        MangaKakalotConnector {
            client: ThrottledClient::new(Some(headers), Duration::from_secs(1), 5),
            search_url: Url::parse("https://mangakakalot.com/search/story/").unwrap(),
            manga_url: Url::parse("https://chapmanganato.to").unwrap(),
            manga_alias_url: Url::parse("https://chapmanganato.to").unwrap(),
            search_manga_selector: util::make_selector(".panel_story_list>.story_item>a"),
            cover_selector: util::make_selector(".container .info-image img"),
            desc_selector: util::make_selector("#panel-story-info-description"),
            status_selector: util::make_selector("table>tbody>tr:nth-child(3)>.table-value"),
            chap_title_selector: util::make_selector(".row-content-chapter>li>a.chapter-name"),
            chap_date_selector: util::make_selector(".row-content-chapter>li>.chapter-time"),
            image_selector: util::make_selector(".container-chapter-reader>img"),
        }
    }

    fn parse_date(&self, date: &str) -> YomugamiResult<String> {
        chrono::NaiveDateTime::parse_from_str(date, "%b %-d, %Y %H:%M")
            .ok()
            .and_then(|naive_datetime| {
                Some(chrono::Utc.from_utc_datetime(&naive_datetime).to_rfc3339())
            })
            .ok_or_else(|| ParseError(format!("Failed to parse date {}", date)))
    }

    fn parse_manga_preview_element(&self, element: &ElementRef) -> YomugamiResult<MangaPreview> {
        let manga_link = util::parse_url(&element, "href", "manga")?;
        let id = util::get_segment(&self.manga_alias_url, &manga_link)?;
        let image_element = element
            .first_element_child()
            .ok_or_else(|| ParseError("Failed to parse image element".to_string()))?;
        let cover_url = util::parse_url(&image_element, "src", "cover")?;
        let cover = futures::executor::block_on(self.client.cache_url(cover_url));
        let title = util::get_attribute(&image_element, "alt")?;

        Ok(MangaPreview { id, title, cover })
    }

    fn parse_chapter_preview_element(
        &self,
        title_element: &ElementRef,
        date_element: &ElementRef,
    ) -> YomugamiResult<ChapterPreview> {
        let chapter_link = util::parse_url(&title_element, "href", "chapter")?;
        let id = util::get_segment(&self.manga_url, &chapter_link)?;
        let raw_date = util::get_attribute(&date_element, "title")?;
        let date = self.parse_date(&raw_date)?;
        let title = util::inner_text(&title_element);

        Ok(ChapterPreview { id, title, date })
    }
}

#[async_trait]
impl Connector for MangaKakalotConnector {
    async fn search_manga(&self, manga_title: &str) -> YomugamiResult<MangaPreviewList> {
        let cleaned_manga_title: String = manga_title
            .chars()
            .filter(|c| c.is_alphanumeric() || c.is_whitespace())
            .map(|c| if c.is_whitespace() { '_' } else { c })
            .collect();

        if cleaned_manga_title.len() < 3 {
            return Err(BadRequest("Manga title is too short".to_string()));
        }

        let url = util::construct_url(&self.search_url, &[&cleaned_manga_title], &[])?;
        let document = self.client.get_html(url).await?;

        document
            .select(&self.search_manga_selector)
            .map(|element| self.parse_manga_preview_element(&element))
            .collect::<YomugamiResult<Vec<MangaPreview>>>()
            .and_then(|mangas| Ok(MangaPreviewList { mangas }))
    }

    async fn fetch_manga(&self, manga_id: &str) -> YomugamiResult<Manga> {
        let url = util::construct_url(&self.manga_url, &[manga_id], &[])?;
        let document = self.client.get_html(url).await?;

        let id = manga_id.to_string();
        let title = util::parse_attribute(&document, &self.cover_selector, "alt", "title")?;

        let raw_cover_url = util::parse_attribute(&document, &self.cover_selector, "src", "cover")?;
        let cover_url = Url::parse(&raw_cover_url)
            .map_err(|err| ParseError(format!("Failed to parse cover url - {}", err)))?;
        let cover = futures::executor::block_on(self.client.cache_url(cover_url));

        let description = util::parse_text(&document, &self.desc_selector, "description")?;
        let year = 0;
        let status = util::parse_text(&document, &self.status_selector, "manga status")?;
        let chapters = document
            .select(&self.chap_title_selector)
            .zip(document.select(&self.chap_date_selector))
            .map(|(title, date)| self.parse_chapter_preview_element(&title, &date))
            .collect::<YomugamiResult<Vec<ChapterPreview>>>()?;

        Ok(Manga {
            id,
            title,
            cover,
            description,
            year,
            status,
            chapters,
        })
    }

    async fn fetch_chapter(&self, chapter_id: &str) -> YomugamiResult<Chapter> {
        let url = util::construct_url(&self.manga_url, &[chapter_id], &[])?;

        let images_links = self
            .client
            .get_html(url)
            .await?
            .select(&self.image_selector)
            .map(|element| util::parse_url(&element, "src", "image"))
            .collect::<YomugamiResult<Vec<Url>>>()?;
        let images = future::join_all(
            images_links
                .iter()
                .map(|link| self.client.cache_url(link.clone())),
        )
        .await;

        Ok(Chapter { images })
    }

    async fn fetch_image(&self, image_id: String) -> YomugamiResult<Image> {
        self.client.get_image(image_id).await
    }
}

#[cfg(test)]
mod tests {
    use crate::connectors::mangakakalot::MangaKakalotConnector;
    use crate::models::Connector;

    #[tokio::test]
    async fn test_search_manga() {
        let connector = MangaKakalotConnector::new();

        let result = connector.search_manga("Leviathan (Lee Gyuntak)").await;
        assert!(result.is_ok());

        let mangas = result.unwrap().mangas;
        assert_eq!(mangas.len(), 1);
        assert_eq!(mangas[0].id, "manga-dx980680".to_string());
        assert_eq!(mangas[0].title, "Leviathan (Lee Gyuntak)".to_string());
    }

    #[tokio::test]
    async fn test_fetch_manga() {
        let connector = MangaKakalotConnector::new();

        let result = connector.fetch_manga("manga-dx980680").await;
        assert!(result.is_ok());

        let manga = result.unwrap();
        assert_eq!(manga.id, "manga-dx980680".to_string());
        assert_eq!(manga.title, "Leviathan (Lee Gyuntak)".to_string());
        assert_eq!(manga.chapters.len(), 215);
        assert_eq!(
            manga.chapters[0].id,
            "manga-dx980680/chapter-214".to_string()
        );
        assert_eq!(manga.chapters[0].title, "Chapter 214".to_string());
        assert_eq!(
            manga.chapters[0].date,
            "2022-07-13T03:07:00+00:00".to_string()
        );
    }

    #[tokio::test]
    async fn test_fetch_chapter() {
        let connector = MangaKakalotConnector::new();

        let result = connector.fetch_chapter("manga-dx980680/chapter-89").await;
        assert!(result.is_ok());

        let images_ids = result.unwrap().images;
        assert_eq!(images_ids.len(), 108);

        let image = connector.fetch_image(images_ids[0].clone()).await;
        assert!(image.is_ok());
    }
}
