use async_trait::async_trait;
use futures::{future, TryFutureExt};
use reqwest::header::{HeaderMap, HeaderValue, USER_AGENT};
use reqwest::Url;
use tokio::time::Duration;

use crate::connectors::mangadex::mangadex_models::{
    MangaDexEntity, MangaDexFetchChapterResponse, MangaDexFetchChaptersResponse,
    MangaDexFetchMangaResponse, MangaDexRelationship, MangaDexSearchMangaResponse,
};
use crate::connectors::throttled_client::ThrottledClient;
use crate::connectors::util;
use crate::error::YomugamiError::{ConnectorFailure, MangaUnavailable, ParseError};
use crate::error::YomugamiResult;
use crate::models::{
    Chapter, ChapterPreview, Connector, Image, Manga, MangaPreview, MangaPreviewList,
};

mod mangadex_models;

pub struct MangaDexConnector {
    client: ThrottledClient,
    manga_url: Url,
    cover_url: Url,
    chapter_url: Url,
}

impl MangaDexConnector {
    pub fn new() -> Self {
        let mut headers = HeaderMap::new();
        headers.insert(
            USER_AGENT,
            HeaderValue::from_static(
                "Mozilla/5.0 (X11; Linux x86_64; rv:130.0) Gecko/20100101 Firefox/130.0",
            ),
        );

        MangaDexConnector {
            client: ThrottledClient::new(Some(headers), Duration::from_secs(1), 5),
            manga_url: Url::parse("https://api.mangadex.org/manga/").unwrap(),
            cover_url: Url::parse("https://uploads.mangadex.org/covers/").unwrap(),
            chapter_url: Url::parse("https://api.mangadex.org/at-home/server/").unwrap(),
        }
    }

    pub(super) async fn fetch_all_chapters(
        &self,
        manga_id: &str,
    ) -> YomugamiResult<Vec<ChapterPreview>> {
        let mut url = util::construct_url(
            &self.manga_url,
            &[&util::add_trailing_slash(manga_id), "feed"],
            &[],
        )?;
        let limit = 500;
        let mut chapters: Vec<ChapterPreview> = vec![];
        let mut offset = 0;

        for _ in 0..10 {
            url.query_pairs_mut()
                .clear()
                .append_pair("limit", &limit.to_string())
                .append_pair("offset", &offset.to_string())
                .append_pair("translatedLanguage[]", "en")
                .append_pair("order[chapter]", "desc");
            let fetched_chapters = self
                .client
                .get_json::<MangaDexFetchChaptersResponse>(url.clone())
                .await?;

            offset = fetched_chapters.offset + limit;
            let remaining = fetched_chapters.total - offset;

            chapters.extend(
                fetched_chapters
                    .data
                    .iter()
                    .map(|chap| self.mangadex_to_chapter_preview(&chap))
                    .collect::<YomugamiResult<Vec<ChapterPreview>>>()?,
            );

            if remaining <= 0 {
                return Ok(chapters);
            }
        }

        Err(ConnectorFailure(
            "Failed to retrieve all the chapters".to_string(),
        ))
    }

    fn mangadex_to_manga_preview(&self, mangadex: &MangaDexEntity) -> YomugamiResult<MangaPreview> {
        let MangaDexEntity::Manga {
            id,
            attributes: Some(attributes),
            relationships,
        } = mangadex
        else {
            return Err(ParseError(
                "Failed to map mangadex entity to manga".to_string(),
            ));
        };
        let title = attributes.title.default()?.to_string();
        let cover_name = relationships
            .iter()
            .find_map(|r| {
                let MangaDexRelationship::Cover {
                    attributes: Some(attributes),
                    ..
                } = r
                else {
                    return None;
                };
                Some(&attributes.file_name)
            })
            .ok_or_else(|| MangaUnavailable("Cover relationship not found".to_string()))?
            .clone();

        let cover_url = util::construct_url(
            &self.cover_url,
            &[&util::add_trailing_slash(&id), &cover_name],
            &[],
        )?;
        let cover = futures::executor::block_on(self.client.cache_url(cover_url));

        Ok(MangaPreview {
            id: id.clone(),
            title,
            cover,
        })
    }

    async fn mangadex_to_manga(&self, mangadex: MangaDexEntity) -> YomugamiResult<Manga> {
        let MangaPreview { id, title, cover } = self.mangadex_to_manga_preview(&mangadex)?;
        let MangaDexEntity::Manga {
            attributes: Some(attributes),
            ..
        } = mangadex
        else {
            return Err(ParseError(
                "Failed to map mangadex entity to manga".to_string(),
            ));
        };
        let description = attributes.description.default()?.to_string();
        let year = attributes.year.unwrap_or(0_u32);
        let status = attributes.status;
        let chapters = self.fetch_all_chapters(&id).await?;

        Ok(Manga {
            id,
            title,
            cover,
            description,
            year,
            status,
            chapters,
        })
    }

    fn mangadex_to_chapter_preview(
        &self,
        mangadex: &MangaDexEntity,
    ) -> YomugamiResult<ChapterPreview> {
        let MangaDexEntity::Chapter {
            id,
            attributes: Some(attributes),
            ..
        } = mangadex
        else {
            return Err(ParseError(
                "Failed to map mangadex entity to chapter preview".to_string(),
            ));
        };
        let id = id.clone();
        let title = attributes
            .chapter
            .clone()
            .ok_or_else(|| ParseError("Chapter title not found".to_string()))?;
        let date = attributes.created_at.clone();

        Ok(ChapterPreview { id, title, date })
    }

    async fn mangadex_to_chapter(
        &self,
        mangadex: MangaDexFetchChapterResponse,
    ) -> YomugamiResult<Chapter> {
        let base = Url::parse(&mangadex.base_url)
            .map_err(|err| ParseError(format!("Failed to parse base url - {}", err)))?;
        let url = util::construct_url(
            &base,
            &["data/", &util::add_trailing_slash(&mangadex.chapter.hash)],
            &[],
        )?;

        let images_links = mangadex
            .chapter
            .data
            .iter()
            .map(|filename| {
                url.join(filename)
                    .map_err(|err| ParseError(format!("Failed to parse chapter url - {}", err)))
            })
            .collect::<YomugamiResult<Vec<Url>>>()?;
        let images = future::join_all(
            images_links
                .iter()
                .map(|link| self.client.cache_url(link.clone())),
        )
        .await;

        Ok(Chapter { images })
    }
}

#[async_trait]
impl Connector for MangaDexConnector {
    async fn search_manga(&self, manga_title: &str) -> YomugamiResult<MangaPreviewList> {
        let url = util::construct_url(
            &self.manga_url,
            &[],
            &[("includes[]", "cover_art"), ("title", manga_title)],
        )?;

        let res = self
            .client
            .get_json::<MangaDexSearchMangaResponse>(url)
            .await?;

        res.data
            .iter()
            .filter_map(|manga| match self.mangadex_to_manga_preview(manga) {
                Err(MangaUnavailable(_)) => None,
                manga_preview => Some(manga_preview),
            })
            .collect::<YomugamiResult<Vec<MangaPreview>>>()
            .and_then(|mangas| Ok(MangaPreviewList { mangas }))
    }

    async fn fetch_manga(&self, manga_id: &str) -> YomugamiResult<Manga> {
        let url =
            util::construct_url(&self.manga_url, &[manga_id], &[("includes[]", "cover_art")])?;
        let res = self
            .client
            .get_json::<MangaDexFetchMangaResponse>(url)
            .await?;

        self.mangadex_to_manga(res.data).await
    }

    async fn fetch_chapter(&self, chapter_id: &str) -> YomugamiResult<Chapter> {
        let url = util::construct_url(&self.chapter_url, &[chapter_id], &[])?;
        let res = self
            .client
            .get_json::<MangaDexFetchChapterResponse>(url)
            .await?;

        self.mangadex_to_chapter(res).await
    }

    async fn fetch_image(&self, image_id: String) -> YomugamiResult<Image> {
        self.client.get_image(image_id).await
    }
}

#[cfg(test)]
mod tests {
    use crate::connectors::mangadex::MangaDexConnector;
    use crate::models::Connector;

    #[tokio::test]
    async fn test_search_manga() {
        let connector = MangaDexConnector::new();

        let result = connector.search_manga("Martial Artist Lee Gwak").await;
        assert!(result.is_ok());

        let mangas = result.unwrap().mangas;
        assert_eq!(mangas.len(), 1);
        assert_eq!(
            mangas[0].id,
            "ffe650da-5dd4-480b-9f52-e43b53d1daec".to_string()
        );
        assert_eq!(mangas[0].title, "Martial Artist Lee Gwak".to_string());
    }

    #[tokio::test]
    async fn test_fetch_manga() {
        let connector = MangaDexConnector::new();

        let result = connector
            .fetch_manga("d1a9fdeb-f713-407f-960c-8326b586e6fd")
            .await;
        assert!(result.is_ok());

        let manga = result.unwrap();
        assert_eq!(manga.id, "d1a9fdeb-f713-407f-960c-8326b586e6fd".to_string());
        assert_eq!(manga.title, "Vagabond".to_string());
        assert_eq!(manga.chapters.len(), 114);
        assert_eq!(
            manga.chapters[0].id,
            "0754c218-0240-4752-a688-5e7d9bc74b55".to_string()
        );
        assert_eq!(manga.chapters[0].title, "327".to_string());
        assert_eq!(
            manga.chapters[0].date,
            "2018-03-19T02:20:43+00:00".to_string()
        );
    }

    #[tokio::test]
    async fn test_fetch_chapter() {
        let connector = MangaDexConnector::new();

        let result = connector
            .fetch_chapter("0754c218-0240-4752-a688-5e7d9bc74b55")
            .await;
        assert!(result.is_ok());

        let images_ids = result.unwrap().images;
        assert_eq!(images_ids.len(), 19);

        let image = connector.fetch_image(images_ids[0].clone()).await;
        assert!(image.is_ok());
    }
}
