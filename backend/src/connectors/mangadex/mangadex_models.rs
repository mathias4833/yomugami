use crate::error::YomugamiError::MangaUnavailable;
use crate::error::YomugamiResult;
use serde::Deserialize;
use std::collections::HashMap;

#[derive(Deserialize)]
pub struct LocalizedString(HashMap<String, String>);

impl LocalizedString {
    pub fn get(&self, lang: &str) -> Option<&String> {
        self.0.get(lang)
    }

    pub fn default(&self) -> YomugamiResult<&String> {
        self.get("en")
            .ok_or_else(|| MangaUnavailable("No english translation found".to_string()))
    }
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MangaDexCoverAttributes {
    pub volume: Option<String>,
    pub file_name: String,
    pub description: Option<String>,
    pub locale: Option<String>,
    pub version: u8,
    pub created_at: String,
    pub updated_at: String,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MangaDexMangaAttributes {
    pub title: LocalizedString,
    pub alt_titles: Vec<LocalizedString>,
    pub description: LocalizedString,
    pub is_locked: bool,
    pub links: LocalizedString,
    pub original_language: String,
    pub last_volume: Option<String>,
    pub last_chapter: Option<String>,
    pub publication_demographic: Option<String>,
    pub status: String,
    pub year: Option<u32>,
    pub content_rating: String,
    pub chapter_numbers_reset_on_new_volume: bool,
    pub available_translated_languages: Vec<String>,
    pub latest_uploaded_chapter: Option<String>,
    pub tags: Vec<MangaDexEntity>,
    pub state: String,
    pub version: u8,
    pub created_at: String,
    pub updated_at: String,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MangaDexChapterAttributes {
    pub title: Option<String>,
    pub volume: Option<String>,
    pub chapter: Option<String>,
    pub pages: u16,
    pub translated_language: String,
    pub uploader: Option<String>,
    pub external_url: Option<String>,
    pub version: u8,
    pub created_at: String,
    pub updated_at: String,
    pub publish_at: String,
    pub readable_at: String,
}

#[derive(Deserialize)]
#[serde(tag = "type")]
pub enum MangaDexEntity {
    #[serde(rename = "cover_art")]
    Cover {
        id: String,
        attributes: Option<MangaDexCoverAttributes>,
        relationships: Vec<MangaDexRelationship>,
    },
    #[serde(rename = "manga")]
    Manga {
        id: String,
        attributes: Option<MangaDexMangaAttributes>,
        relationships: Vec<MangaDexRelationship>,
    },
    #[serde(rename = "chapter")]
    Chapter {
        id: String,
        attributes: Option<MangaDexChapterAttributes>,
        relationships: Vec<MangaDexRelationship>,
    },
    #[serde(other)]
    Other,
}

#[derive(Deserialize)]
#[serde(tag = "type")]
pub enum MangaDexRelationship {
    #[serde(rename = "cover_art")]
    Cover {
        id: String,
        related: Option<String>,
        attributes: Option<MangaDexCoverAttributes>,
    },
    #[serde(rename = "manga")]
    Manga {
        id: String,
        related: Option<String>,
        attributes: Option<MangaDexMangaAttributes>,
    },
    #[serde(rename = "chapter")]
    Chapter {
        id: String,
        related: Option<String>,
        attributes: Option<MangaDexChapterAttributes>,
    },
    #[serde(other)]
    Other,
}

#[derive(Deserialize)]
pub struct MangaDexSearchMangaResponse {
    pub result: String,
    pub response: String,
    pub data: Vec<MangaDexEntity>,
    pub limit: i32,
    pub offset: i32,
    pub total: i32,
}

#[derive(Deserialize)]
pub struct MangaDexFetchChaptersResponse {
    pub result: String,
    pub response: String,
    pub data: Vec<MangaDexEntity>,
    pub limit: i32,
    pub offset: i32,
    pub total: i32,
}

#[derive(Deserialize)]
pub struct MangaDexFetchMangaResponse {
    pub result: String,
    pub response: String,
    pub data: MangaDexEntity,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MangaDexChapterData {
    pub hash: String,
    pub data: Vec<String>,
    pub data_saver: Vec<String>,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MangaDexFetchChapterResponse {
    pub result: String,
    pub base_url: String,
    pub chapter: MangaDexChapterData,
}
