use crate::connectors::cache::UrlCache;
use crate::error::YomugamiError::{BadRequest, ConnectorFailure, ParseError};
use crate::error::YomugamiResult;
use crate::models::Image;
use reqwest::header::{HeaderMap, CONTENT_TYPE};
use reqwest::{Client, Response, Url};
use scraper::Html;
use serde::de::DeserializeOwned;
use std::sync::Arc;
use std::time::Duration;
use tokio::sync::Mutex;
use tokio::time::{sleep, Instant};

pub struct ThrottledClient {
    client: Client,
    cache: Arc<UrlCache>,
    tokens: Mutex<i32>,
    last_request: Mutex<Instant>,
    refill_rate: Duration,
    max_tokens: i32,
}

impl ThrottledClient {
    pub fn new(headers: Option<HeaderMap>, refill_rate: Duration, max_tokens: i32) -> Self {
        let client = match headers {
            Some(headers) => Client::builder().default_headers(headers).build().unwrap(),
            None => Client::new(),
        };

        ThrottledClient {
            client,
            cache: UrlCache::new(Duration::from_secs(60 * 15), Duration::from_secs(60)),
            tokens: Mutex::new(max_tokens),
            last_request: Mutex::new(Instant::now()),
            refill_rate,
            max_tokens,
        }
    }

    async fn update_tokens(&self) {
        let elapsed = self.last_request.lock().await.elapsed();
        let tokens_to_add = (elapsed.as_secs_f32() / self.refill_rate.as_secs_f32()) as i32;
        let mut tokens = self.tokens.lock().await;
        *tokens = (*tokens + tokens_to_add).min(self.max_tokens);
    }

    async fn throttle(&self) {
        while *self.tokens.lock().await <= 0 {
            sleep(Duration::from_millis(100)).await;
            self.update_tokens().await;
        }

        *self.tokens.lock().await -= 1;
    }

    pub async fn get(&self, url: Url) -> YomugamiResult<Response> {
        self.throttle().await;

        let response =
            self.client.get(url).send().await.map_err(|err| {
                ConnectorFailure(format!("Failed to establish connection - {}", err))
            })?;

        *self.last_request.lock().await = Instant::now();
        self.update_tokens().await;

        Ok(response)
    }

    pub async fn get_html(&self, url: Url) -> YomugamiResult<Html> {
        let res = self.get(url).await?;
        let text = res
            .text()
            .await
            .map_err(|err| ConnectorFailure(format!("Failed to get response text - {}", err)))?;
        Ok(Html::parse_document(&text))
    }

    pub async fn get_json<T: DeserializeOwned>(&self, url: Url) -> YomugamiResult<T> {
        let res = self.get(url).await?;
        res.json::<T>()
            .await
            .map_err(|err| ParseError(format!("Failed to parse json response - {}", err)))
    }

    pub async fn cache_url(&self, url: Url) -> String {
        self.cache.add_url(url).await
    }

    pub async fn get_image(&self, image_id: String) -> YomugamiResult<Image> {
        let url = self
            .cache
            .get_url(image_id)
            .await
            .ok_or_else(|| BadRequest("Invalid image id".to_string()))?;

        let res = self.get(url).await?;
        let content_type = res
            .headers()
            .get(CONTENT_TYPE)
            .and_then(|ct| ct.to_str().ok())
            .ok_or_else(|| ConnectorFailure("Failed to retrieve image content type".to_string()))?
            .to_string();

        if !content_type.starts_with("image/") {
            return Err(ConnectorFailure("Invalid image content type".to_string()));
        }

        Ok(Image {
            content_type,
            stream: Box::pin(res.bytes_stream()),
        })
    }
}

#[cfg(test)]
mod tests {
    use crate::connectors::throttled_client::ThrottledClient;
    use reqwest::Url;
    use std::time::Duration;
    use tokio::time::Instant;

    #[tokio::test]
    async fn test_throttle() {
        let refill_rate = Duration::from_millis(500);
        let client = ThrottledClient::new(None, refill_rate, 3);

        let tokens = *client.tokens.lock().await;
        assert_eq!(tokens, 3);

        client.throttle().await;

        let tokens = *client.tokens.lock().await;
        assert_eq!(tokens, 2);

        client.throttle().await;
        client.throttle().await;

        let tokens = *client.tokens.lock().await;
        assert_eq!(tokens, 0);

        let start_time = Instant::now();
        client.throttle().await;
        let elapsed = start_time.elapsed();

        assert!(refill_rate <= elapsed);
        assert!(elapsed <= refill_rate + Duration::from_millis(100));
    }

    #[tokio::test]
    async fn test_get() {
        let refill_rate = Duration::from_millis(500);
        let max_tokens = 3;
        let client = ThrottledClient::new(None, refill_rate, max_tokens);

        let start_time = Instant::now();
        std::thread::sleep(Duration::from_millis(200));

        let url = Url::parse("https://example.com").unwrap();
        let response = client.get(url.clone()).await;

        assert!(response.is_ok());
        assert!(response.unwrap().error_for_status().is_ok());

        let tokens = *client.tokens.lock().await;
        assert_eq!(tokens, max_tokens - 1);

        let last_request = *client.last_request.lock().await;
        assert!(last_request.elapsed() < start_time.elapsed());
    }
}
