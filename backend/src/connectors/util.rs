use crate::error::YomugamiError::ParseError;
use lazy_static::lazy_static;
use reqwest::Url;
use scraper::{ElementRef, Html, Selector};

use crate::error::YomugamiResult;

lazy_static! {
    static ref CANONICAL_URL: Selector = make_selector("link[rel='canonical']");
}

pub fn make_selector(selector: &str) -> Selector {
    Selector::parse(selector).unwrap()
}

pub fn add_trailing_slash(path: &str) -> String {
    if path.ends_with("/") {
        path.to_string()
    } else {
        path.to_string() + "/"
    }
}

pub fn get_segment(base: &Url, url: &Url) -> YomugamiResult<String> {
    url.as_str()
        .strip_prefix(base.as_str())
        .and_then(|rel| Some(rel.trim_matches('/').to_string()))
        .ok_or_else(|| ParseError("Failed to create relative url".to_string()))
}

pub fn construct_url(
    base: &Url,
    segments: &[&str],
    queries: &[(&str, &str)],
) -> YomugamiResult<Url> {
    let mut url = segments.iter().try_fold(base.clone(), |acc, &s| {
        Ok(acc
            .join(s)
            .map_err(|err| ParseError(format!("Failed to construct url - {}", err)))?)
    })?;

    if !queries.is_empty() {
        queries
            .iter()
            .fold(&mut url.query_pairs_mut(), |acc, &(k, v)| {
                acc.append_pair(k, v)
            });
    }
    Ok(url)
}

pub fn inner_text(element: &ElementRef) -> String {
    element.text().collect::<String>().trim().to_string()
}

pub fn get_attribute(element: &ElementRef, attr: &str) -> YomugamiResult<String> {
    element
        .value()
        .attr(attr)
        .and_then(|attribute| Some(attribute.trim().to_string()))
        .ok_or_else(|| ParseError(format!("Failed to parse {} attribute", attr)))
}

pub fn get_canonical_url(document: &Html) -> YomugamiResult<Url> {
    Url::parse(&parse_attribute(
        document,
        &CANONICAL_URL,
        "href",
        "Canonical url",
    )?)
    .map_err(|err| ParseError(format!("Failed to parse canonical url - {}", err)))
}

pub fn parse_text(document: &Html, selector: &Selector, err: &str) -> YomugamiResult<String> {
    Ok(inner_text(&document.select(selector).next().ok_or_else(
        || ParseError(format!("Failed to parse {}", err)),
    )?))
}

pub fn parse_attribute(
    document: &Html,
    selector: &Selector,
    attr: &str,
    err: &str,
) -> YomugamiResult<String> {
    let element = document
        .select(selector)
        .next()
        .ok_or_else(|| ParseError(format!("Failed to parse {}", err)))?;
    get_attribute(&element, attr)
}

pub fn parse_url(element: &ElementRef, attr: &str, err_element: &str) -> YomugamiResult<Url> {
    Url::parse(&get_attribute(element, attr)?)
        .map_err(|err| ParseError(format!("Failed to parse {} url - {}", err_element, err)))
}

#[cfg(test)]
mod tests {
    use crate::connectors::util::{
        add_trailing_slash, construct_url, get_attribute, get_canonical_url, get_segment,
        inner_text, parse_attribute, parse_text,
    };
    use reqwest::Url;
    use scraper::{Element, Html, Selector};

    #[test]
    fn test_add_trailing_slash_with_slash() {
        let path = "path/with/slash/";
        let result = add_trailing_slash(path);
        assert_eq!(result, path);
    }

    #[test]
    fn test_add_trailing_slash_without_slash() {
        let path = "path/without/slash";
        let expected = "path/without/slash/";
        let result = add_trailing_slash(path);
        assert_eq!(result, expected)
    }

    #[test]
    fn test_get_segment_parent() {
        let base = Url::parse("https://example.com/some/path/").unwrap();
        let url = Url::parse("https://example.com/some/path/to/index.html").unwrap();
        let expected_result = "to/index.html".to_string();
        let result = get_segment(&base, &url).ok();
        assert_eq!(result, Some(expected_result));
    }

    #[test]
    fn test_get_segment_not_parent() {
        let base = Url::parse("https://example.com/some/path/").unwrap();
        let url = Url::parse("https://example.com/another/path/to/index.html").unwrap();
        let result = get_segment(&base, &url);
        assert!(result.is_err());
    }

    #[test]
    fn test_get_segment_same_url() {
        let url = Url::parse("https://example.com/some/path/").unwrap();
        let expected_result = "".to_string();
        let result = get_segment(&url, &url).ok();
        assert_eq!(result, Some(expected_result));
    }

    #[test]
    fn test_construct_url_valid() {
        let base = Url::parse("https://example.com").unwrap();
        let segments = &["some/", "path"];
        let queries = &[("key1", "value1"), ("key2", "value2")];
        let expected_result = "https://example.com/some/path?key1=value1&key2=value2".to_string();
        let result = construct_url(&base, segments, queries).ok();
        assert_eq!(result.map(|u| u.to_string()), Some(expected_result));
    }

    #[test]
    fn test_construct_url_empty_segments() {
        let base = Url::parse("https://example.com").unwrap();
        let segments: &[&str] = &[];
        let queries = &[("key1", "value1"), ("key2", "value2")];
        let expected_result = "https://example.com/?key1=value1&key2=value2".to_string();
        let result = construct_url(&base, segments, queries).ok();
        assert_eq!(result.map(|u| u.to_string()), Some(expected_result));
    }

    #[test]
    fn test_construct_url_empty_queries() {
        let base = Url::parse("https://example.com").unwrap();
        let segments = &["some/", "path"];
        let queries: &[(&str, &str)] = &[];
        let expected_result = "https://example.com/some/path".to_string();
        let result = construct_url(&base, segments, queries).ok();
        assert_eq!(result.map(|u| u.to_string()), Some(expected_result));
    }

    #[test]
    fn test_inner_text_without_child_elements() {
        let fragment = Html::parse_fragment("Hello, world!");
        let element = fragment.root_element();
        let expected_result = "Hello, world!".to_string();
        let result = inner_text(&element);
        assert_eq!(result, expected_result);
    }

    #[test]
    fn test_inner_text_with_child_elements() {
        let fragment = Html::parse_fragment("<h1>Hello, <i>world!</i></h1>");
        let element = fragment.root_element();
        let expected_result = "Hello, world!".to_string();
        let result = inner_text(&element);
        assert_eq!(result, expected_result);
    }

    #[test]
    fn test_get_attribute_existing_attribute() {
        let fragment = Html::parse_fragment("<a href=\"https://example.com\">Text</a>");
        let element = fragment.root_element().first_element_child().unwrap();
        let expected_result = "https://example.com".to_string();
        let result = get_attribute(&element, "href").ok();
        assert_eq!(result, Some(expected_result));
    }

    #[test]
    fn test_get_attribute_non_existing_attribute() {
        let fragment = Html::parse_fragment("<a>Text</a>");
        let element = fragment.root_element().first_element_child().unwrap();
        let result = get_attribute(&element, "href");
        assert!(result.is_err());
    }

    #[test]
    fn test_get_attribute_value_with_whitespace() {
        let fragment = Html::parse_fragment("<a href=\"  https://example.com  \">Text</a>");
        let element = fragment.root_element().first_element_child().unwrap();
        let expected_result = "https://example.com".to_string();
        let result = get_attribute(&element, "href").ok();
        assert_eq!(result, Some(expected_result));
    }

    #[test]
    fn test_get_canonical_url_valid() {
        let document = Html::parse_document(
            "<!DOCTYPE html>
            <html>
            <head>
                <link rel=\"canonical\" href=\"https://example.com\" />
            </head>
            <body>
            </body>
            </html>",
        );
        let expected_result = "https://example.com/".to_string();
        let result = get_canonical_url(&document).ok();
        assert_eq!(result.map(|u| u.to_string()), Some(expected_result));
    }

    #[test]
    fn test_get_canonical_url_missing_url() {
        let document = Html::parse_document(
            "<!DOCTYPE html>
            <html>
            <head>
            </head>
            <body>
            </body>
            </html>",
        );
        let result = get_canonical_url(&document);
        assert!(result.is_err());
    }

    #[test]
    fn test_get_canonical_url_invalid_url() {
        let document = Html::parse_document(
            "<!DOCTYPE html>
            <html>
            <head>
                <link rel=\"canonical\" href=\"invalid url\" />
            </head>
            <body>
            </body>
            </html>",
        );
        let result = get_canonical_url(&document);
        assert!(result.is_err());
    }

    #[test]
    fn test_parse_text_valid() {
        let document = Html::parse_fragment("<h1>Hello, <i>world!</i></h1>");
        let selector = Selector::parse("i").unwrap();
        let expected_result = "world!".to_string();
        let result = parse_text(&document, &selector, "").ok();
        assert_eq!(result, Some(expected_result));
    }

    #[test]
    fn test_parse_text_invalid() {
        let document = Html::parse_fragment("<h1>Hello, <i>world!</i></h1>");
        let selector = Selector::parse("div").unwrap();
        let result = parse_text(&document, &selector, "");
        assert!(result.is_err());
    }

    #[test]
    fn test_parse_attribute_valid() {
        let document =
            Html::parse_fragment("<h1>Hello, <a href=\"https://example.com\">world</a></h1>");
        let selector = Selector::parse("a").unwrap();
        let expected_result = "https://example.com".to_string();
        let result = parse_attribute(&document, &selector, "href", "").ok();
        assert_eq!(result, Some(expected_result));
    }

    #[test]
    fn test_parse_attribute_invalid_selector() {
        let document =
            Html::parse_fragment("<h1>Hello, <a href=\"https://example.com\">world</a></h1>");
        let selector = Selector::parse("div").unwrap();
        let attribute = "href";
        let result = parse_attribute(&document, &selector, attribute, "");
        assert!(result.is_err());
    }

    #[test]
    fn test_parse_attribute_invalid_attribute() {
        let document =
            Html::parse_fragment("<h1>Hello, <a href=\"https://example.com\">world</a></h1>");
        let selector = Selector::parse("a").unwrap();
        let attribute = "invalid";
        let result = parse_attribute(&document, &selector, attribute, "");
        assert!(result.is_err());
    }
}
