use actix_web::http::header::{HeaderValue, REFERER};
use async_trait::async_trait;
use chrono::{SubsecRound, TimeZone};
use futures::future;
use regex::Regex;
use reqwest::header::HeaderMap;
use reqwest::Url;
use scraper::{Element, ElementRef, Selector};
use tokio::time::Duration;

use crate::connectors::throttled_client::ThrottledClient;
use crate::connectors::util;
use crate::error::YomugamiError::ParseError;
use crate::error::YomugamiResult;
use crate::models::{
    Chapter, ChapterPreview, Connector, Image, Manga, MangaPreview, MangaPreviewList,
};

pub struct MangaRockTeamConnector {
    client: ThrottledClient,
    base_url: Url,
    manga_url: Url,
    relative_date_re: Regex,
    search_cover_selector: Selector,
    search_title_selector: Selector,
    cover_selector: Selector,
    title_selector: Selector,
    desc_selector: Selector,
    year_selector: Selector,
    status_selector: Selector,
    chap_selector: Selector,
    image_selector: Selector,
}

impl MangaRockTeamConnector {
    pub fn new() -> Self {
        let mut headers = HeaderMap::new();
        headers.insert(
            REFERER,
            HeaderValue::from_static("https://mangarockteam.com/"),
        );

        MangaRockTeamConnector {
            client: ThrottledClient::new(Some(headers), Duration::from_secs(1), 5),
            base_url: Url::parse("https://mangarockteam.com").unwrap(),
            manga_url: Url::parse("https://mangarockteam.com/manga/").unwrap(),
            relative_date_re: Regex::new(r"^(\d+) (\w+) ago$").unwrap(),
            search_cover_selector: util::make_selector(".c-tabs-item .c-image-hover img"),
            search_title_selector: util::make_selector(".c-tabs-item .post-title a"),
            cover_selector: util::make_selector(".summary_image img"),
            title_selector: util::make_selector(".post-title>h1"),
            desc_selector: util::make_selector(".summary__content"),
            year_selector: util::make_selector(
                ".post-status .post-content_item:nth-child(1) .summary-content",
            ),
            status_selector: util::make_selector(
                ".post-status .post-content_item:nth-child(2) .summary-content",
            ),
            chap_selector: util::make_selector("li.wp-manga-chapter"),
            image_selector: util::make_selector(".read-container .page-break>img"),
        }
    }

    fn parse_relative_date(&self, date: &str) -> YomugamiResult<String> {
        self.relative_date_re
            .captures(date)
            .and_then(|caps| {
                let num = caps[1].parse::<i64>().ok()?;
                let duration = match &caps[2] {
                    "mins" | "min" => chrono::Duration::minutes(num),
                    "hours" | "hour" => chrono::Duration::hours(num),
                    "days" | "day" => chrono::Duration::days(num),
                    _ => return None,
                };
                let local_datetime = chrono::Local::now().round_subsecs(0) - duration;
                let utc_datetime = local_datetime.with_timezone(&chrono::Utc);
                Some(utc_datetime.to_rfc3339())
            })
            .ok_or_else(|| ParseError(format!("Failed to parse date {}", date)))
    }

    fn parse_absolute_date(&self, date: &str) -> YomugamiResult<String> {
        chrono::NaiveDate::parse_from_str(date, "%B %-d, %Y")
            .ok()
            .and_then(|naive_date| naive_date.and_hms_opt(0, 0, 0))
            .and_then(|naive_datetime| {
                Some(chrono::Utc.from_utc_datetime(&naive_datetime).to_rfc3339())
            })
            .ok_or_else(|| ParseError(format!("Failed to parse date {}", date)))
    }

    fn parse_manga_preview_element(
        &self,
        title_element: &ElementRef,
        cover_element: &ElementRef,
    ) -> YomugamiResult<MangaPreview> {
        let title = util::inner_text(&title_element);
        let cover_url = util::parse_url(&cover_element, "data-src", "cover")?;
        let cover = futures::executor::block_on(self.client.cache_url(cover_url));
        let manga_link = util::parse_url(&title_element, "href", "manga")?;
        let id = util::get_segment(&self.manga_url, &manga_link)?;

        Ok(MangaPreview { id, title, cover })
    }

    fn parse_chapter_preview_element(
        &self,
        element: &ElementRef,
    ) -> YomugamiResult<ChapterPreview> {
        let title_element = element
            .first_element_child()
            .ok_or_else(|| ParseError("Failed to parse chapter title element".to_string()))?;
        let date_element = title_element
            .next_sibling_element()
            .and_then(|sibling| sibling.first_element_child())
            .ok_or_else(|| ParseError("Failed to parse chapter date element".to_string()))?;

        let date = if date_element.value().name() == "a" {
            let raw_date = util::get_attribute(&date_element, "title")?;
            self.parse_relative_date(&raw_date)?
        } else {
            let raw_date = util::inner_text(&date_element);
            self.parse_absolute_date(&raw_date)?
        };
        let title = util::inner_text(&title_element);
        let chapter_link = util::parse_url(&title_element, "href", "chapter")?;
        let id = util::get_segment(&self.manga_url, &chapter_link)?;

        Ok(ChapterPreview { id, title, date })
    }
}

#[async_trait]
impl Connector for MangaRockTeamConnector {
    async fn search_manga(&self, manga_title: &str) -> YomugamiResult<MangaPreviewList> {
        let url = util::construct_url(
            &self.base_url,
            &[],
            &[("post_type", "wp-manga"), ("s", manga_title)],
        )?;
        let document = self.client.get_html(url).await?;

        document
            .select(&self.search_title_selector)
            .zip(document.select(&self.search_cover_selector))
            .map(|(title, cover)| self.parse_manga_preview_element(&title, &cover))
            .collect::<YomugamiResult<Vec<MangaPreview>>>()
            .and_then(|mangas| Ok(MangaPreviewList { mangas }))
    }

    async fn fetch_manga(&self, manga_id: &str) -> YomugamiResult<Manga> {
        let url = util::construct_url(&self.manga_url, &[manga_id], &[])?;
        let document = self.client.get_html(url).await?;

        let canonical_url = util::get_canonical_url(&document)?;
        let id = util::get_segment(&self.manga_url, &canonical_url)?;
        let title = util::parse_text(&document, &self.title_selector, "title")?;

        let raw_cover_url =
            util::parse_attribute(&document, &self.cover_selector, "data-src", "cover")?;
        let cover_url = Url::parse(&raw_cover_url)
            .map_err(|err| ParseError(format!("Failed to parse cover url - {}", err)))?;
        let cover = futures::executor::block_on(self.client.cache_url(cover_url));

        let description = util::parse_text(&document, &self.desc_selector, "description")?;
        let year = util::parse_text(&document, &self.year_selector, "year")?
            .parse::<u32>()
            .map_err(|err| ParseError(format!("Failed to parse release date - {}", err)))?;
        let status = util::parse_text(&document, &self.status_selector, "status")?;
        let chapters = document
            .select(&self.chap_selector)
            .map(|element| self.parse_chapter_preview_element(&element))
            .collect::<YomugamiResult<Vec<ChapterPreview>>>()?;

        Ok(Manga {
            id,
            title,
            cover,
            description,
            year,
            status,
            chapters,
        })
    }

    async fn fetch_chapter(&self, chapter_id: &str) -> YomugamiResult<Chapter> {
        let url = util::construct_url(&self.manga_url, &[chapter_id], &[])?;

        let images_links = self
            .client
            .get_html(url)
            .await?
            .select(&self.image_selector)
            .map(|element| util::parse_url(&element, "data-src", "image"))
            .collect::<YomugamiResult<Vec<Url>>>()?;
        let images = future::join_all(
            images_links
                .iter()
                .map(|link| self.client.cache_url(link.clone())),
        )
        .await;

        Ok(Chapter { images })
    }

    async fn fetch_image(&self, image_id: String) -> YomugamiResult<Image> {
        self.client.get_image(image_id).await
    }
}

#[cfg(test)]
mod tests {
    use crate::connectors::mangarockteam::MangaRockTeamConnector;
    use crate::models::Connector;

    #[tokio::test]
    async fn test_search_manga() {
        let connector = MangaRockTeamConnector::new();

        let result = connector.search_manga("Leviathan (Lee Gyuntak)").await;
        assert!(result.is_ok());

        let mangas = result.unwrap().mangas;
        assert_eq!(mangas.len(), 1);
        assert_eq!(mangas[0].id, "leviathan-lee-gyuntak".to_string());
        assert_eq!(mangas[0].title, "Leviathan (Lee Gyuntak)".to_string());
    }

    #[tokio::test]
    async fn test_fetch_manga() {
        let connector = MangaRockTeamConnector::new();

        let result = connector.fetch_manga("leviathan-lee-gyuntak").await;
        assert!(result.is_ok());

        let manga = result.unwrap();
        assert_eq!(manga.id, "leviathan-lee-gyuntak".to_string());
        assert_eq!(manga.title, "Leviathan (Lee Gyuntak)".to_string());
        assert_eq!(manga.year, 2018);
        assert_eq!(manga.chapters.len(), 215);
        assert_eq!(
            manga.chapters[0].id,
            "leviathan-lee-gyuntak/chapter-214".to_string()
        );
        assert_eq!(manga.chapters[0].title, "Chapter 214".to_string());
        assert_eq!(
            manga.chapters[0].date,
            "2022-07-14T00:00:00+00:00".to_string()
        );
    }

    #[tokio::test]
    async fn test_fetch_chapter() {
        let connector = MangaRockTeamConnector::new();

        let result = connector
            .fetch_chapter("leviathan-lee-gyuntak/chapter-89")
            .await;
        assert!(result.is_ok());

        let images_ids = result.unwrap().images;
        assert_eq!(images_ids.len(), 108);

        let image = connector.fetch_image(images_ids[0].clone()).await;
        assert!(image.is_ok());
    }
}
