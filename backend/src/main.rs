mod connectors;
mod error;
mod handlers;
mod models;

use crate::connectors::ConnectorsState;
use actix_web::web::Data;
use actix_web::{middleware, App, HttpServer};

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init_from_env(env_logger::Env::new().default_filter_or("info"));

    let state = Data::new(ConnectorsState::new());

    HttpServer::new(move || {
        App::new()
            .app_data(state.clone())
            .wrap(middleware::Logger::default())
            .wrap(middleware::NormalizePath::trim())
            .service(handlers::connectors)
            .service(handlers::search_manga)
            .service(handlers::fetch_manga)
            .service(handlers::fetch_chapter)
            .service(handlers::fetch_image)
    })
    .bind("0.0.0.0:8000")?
    .run()
    .await
}
