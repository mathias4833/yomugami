use crate::error::YomugamiResult;
use actix_web::http::header::{CacheControl, CacheDirective};
use actix_web::{HttpRequest, HttpResponse, Responder};
use async_trait::async_trait;
use bytes::Bytes;
use futures::Stream;
use serde::Serialize;
use std::pin::Pin;

#[async_trait]
pub trait Connector {
    async fn search_manga(&self, manga_title: &str) -> YomugamiResult<MangaPreviewList>;
    async fn fetch_manga(&self, manga_id: &str) -> YomugamiResult<Manga>;
    async fn fetch_chapter(&self, chapter_id: &str) -> YomugamiResult<Chapter>;
    async fn fetch_image(&self, image_id: String) -> YomugamiResult<Image>;
}

#[derive(Serialize)]
pub struct MangaPreview {
    pub id: String,
    pub title: String,
    pub cover: String,
}

#[derive(Serialize)]
pub struct MangaPreviewList {
    pub mangas: Vec<MangaPreview>,
}

impl Responder for MangaPreviewList {
    type Body = actix_web::body::BoxBody;

    fn respond_to(self, _: &HttpRequest) -> HttpResponse<Self::Body> {
        HttpResponse::Ok()
            .insert_header(CacheControl(vec![CacheDirective::MaxAge(60 * 60)]))
            .content_type("application/json; charset=utf-8")
            .json(&self)
    }
}

#[derive(Serialize)]
pub struct ChapterPreview {
    pub id: String,
    pub title: String,
    pub date: String,
}

#[derive(Serialize)]
pub struct Manga {
    pub id: String,
    pub title: String,
    pub cover: String,
    pub description: String,
    pub year: u32,
    pub status: String,
    pub chapters: Vec<ChapterPreview>,
}

impl Responder for Manga {
    type Body = actix_web::body::BoxBody;

    fn respond_to(self, _: &HttpRequest) -> HttpResponse<Self::Body> {
        HttpResponse::Ok()
            .insert_header(CacheControl(vec![CacheDirective::MaxAge(60 * 60)]))
            .content_type("application/json; charset=utf-8")
            .json(&self)
    }
}

#[derive(Serialize)]
pub struct Chapter {
    pub images: Vec<String>,
}

impl Responder for Chapter {
    type Body = actix_web::body::BoxBody;

    fn respond_to(self, _: &HttpRequest) -> HttpResponse<Self::Body> {
        HttpResponse::Ok()
            .insert_header(CacheControl(vec![CacheDirective::MaxAge(60 * 60)]))
            .content_type("application/json; charset=utf-8")
            .json(&self)
    }
}

pub struct Image {
    pub content_type: String,
    pub stream: Pin<Box<dyn Stream<Item = Result<Bytes, reqwest::Error>>>>,
}

impl Responder for Image {
    type Body = actix_web::body::BoxBody;

    fn respond_to(self, _: &HttpRequest) -> HttpResponse<Self::Body> {
        HttpResponse::Ok()
            .insert_header(CacheControl(vec![CacheDirective::MaxAge(60 * 60)]))
            .content_type(self.content_type)
            .streaming(self.stream)
    }
}
