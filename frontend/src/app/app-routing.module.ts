import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { TabsComponent } from './components/tabs/tabs.component';

const routes: Routes = [
  {
    path: '',
    component: TabsComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'library',
      },
      {
        path: 'library',
        loadChildren: () =>
          import('./pages/library/library.module').then(
            (m) => m.LibraryPageModule,
          ),
      },
      {
        path: 'history',
        loadChildren: () =>
          import('./pages/history/history.module').then(
            (m) => m.HistoryPageModule,
          ),
      },
      {
        path: 'explore',
        loadChildren: () =>
          import('./pages/explore/explore.module').then(
            (m) => m.ExplorePageModule,
          ),
      },
    ],
  },
  {
    path: ':connector',
    children: [
      {
        path: 'manga/:mangaId',
        loadChildren: () =>
          import('./pages/manga/manga.module').then((m) => m.MangaPageModule),
      },
      {
        path: 'reader/:mangaId/:chapterId/:pageIndex',
        loadChildren: () =>
          import('./pages/reader/reader.module').then(
            (m) => m.ReaderPageModule,
          ),
      },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
