import { Component, OnInit } from '@angular/core';
import { StorageService } from '../../services/storage.service';
import { YomugamiApiService } from '../../services/yomugami-api.service';
import { Manga, SavedManga } from '../../interfaces/manga';
import { firstValueFrom } from 'rxjs';
import { NotificationToastService } from '../../services/notification-toast.service';
import { SplashService } from '../../services/splash.service';

@Component({
  selector: 'app-library',
  templateUrl: 'library.page.html',
  styleUrls: ['library.page.scss'],
})
export class LibraryPage implements OnInit {
  isLoading: boolean = true;
  isError: boolean = false;
  mangas: Manga[] = [];

  constructor(
    private apiService: YomugamiApiService,
    private toastService: NotificationToastService,
    private splashService: SplashService,
    public storageService: StorageService,
  ) {}

  async ngOnInit(): Promise<void> {
    await this.fetchSavedMangas();
    this.splashService.hideSplash();
  }

  handleRefresh(event: any): void {
    this.fetchSavedMangas(true).finally(() => event.target.complete());
  }

  getImageCover(manga: Manga): string {
    return this.apiService.getImageLink(manga.connector, manga.cover);
  }

  getMangaLink(manga: Manga): string {
    return this.apiService.makeUrl('', manga.connector, 'manga', manga.id);
  }

  async fetchSavedMangas(reload: boolean = false): Promise<void> {
    this.isLoading = true;
    this.isError = false;
    try {
      const savedMangas: SavedManga[] = this.storageService.getSavedMangas();
      this.mangas = await Promise.all(
        savedMangas.map(async (m) =>
          firstValueFrom(this.apiService.fetchManga(m.connector, m.id, reload)),
        ),
      );
    } catch (err) {
      this.isError = true;
      await this.toastService.logError(err);
    }
    this.isLoading = false;
  }
}
