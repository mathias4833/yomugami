import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule, NgOptimizedImage } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LibraryPage } from './library.page';

import { LibraryPageRoutingModule } from './library-routing.module';
import { ImageModule } from '../../components/image/image.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    LibraryPageRoutingModule,
    NgOptimizedImage,
    ImageModule,
  ],
  declarations: [LibraryPage],
})
export class LibraryPageModule {}
