import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChapterPageRoutingModule } from './reader-routing.module';

import { ReaderPage } from './reader.page';
import {ImageModule} from '../../components/image/image.module';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, ChapterPageRoutingModule, ImageModule],
  declarations: [ReaderPage],
})
export class ReaderPageModule {}
