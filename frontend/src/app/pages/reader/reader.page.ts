import {
  Component,
  ElementRef,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { YomugamiApiService } from '../../services/yomugami-api.service';
import { NotificationToastService } from '../../services/notification-toast.service';
import { firstValueFrom } from 'rxjs';
import { Manga } from '../../interfaces/manga';
import { UtilsService } from '../../services/utils.service';
import { Chapter, ChapterData } from '../../interfaces/chapter';
import { IonContent } from '@ionic/angular';
import { SplashService } from '../../services/splash.service';

@Component({
  selector: 'app-reader',
  templateUrl: './reader.page.html',
  styleUrls: ['./reader.page.scss'],
})
export class ReaderPage implements OnInit {
  @ViewChild(IonContent)
  content: IonContent | undefined;

  @ViewChildren('chapterElement', { read: ElementRef })
  chapterElements: QueryList<ElementRef> | undefined;

  @ViewChildren('imageElement', { read: ElementRef })
  imageElements: QueryList<ElementRef> | undefined;

  isError: boolean = false;
  isLoading: boolean = true;
  isToolBarHidden: boolean = false;
  manga: Manga | undefined;
  pageIndex: number | undefined;
  chapters: ChapterData[] = [];
  currentChapter: ChapterData | undefined;
  currentImageIndex: number = 0;

  constructor(
    private utils: UtilsService,
    private route: ActivatedRoute,
    private apiService: YomugamiApiService,
    private toastService: NotificationToastService,
    private splashService: SplashService,
  ) {}

  async ngOnInit(): Promise<void> {
    await this.initReader();
    this.splashService.hideSplash();
  }

  async initReader(): Promise<void> {
    this.isLoading = true;
    this.isError = false;
    try {
      this.manga = await this.fetchManga();

      const chapterId: string = this.utils.getParam(this.route, 'chapterId');
      this.chapters.push(await this.loadChapter(chapterId));
      this.updateCurrentChapter(chapterId);

      this.updateCurrentImage(this.findPageIndex());
    } catch (err) {
      this.isError = true;
      await this.toastService.logError(err);
    }
    this.isLoading = false;
  }

  toggleAppBar(): void {
    if (this.isLoading || this.isError) {
      this.isToolBarHidden = false;
    } else {
      this.isToolBarHidden = !this.isToolBarHidden;
    }
  }

  handlePreviousPressed(): void {
    this.content?.scrollToTop();
  }

  handleNextPressed(): void {
    this.content?.scrollToBottom();
  }

  handleScroll(): void {
    const windowHeight =
      window.innerHeight || document.documentElement.clientHeight;

    const firstVisibleElement = this.chapterElements!.find((element) => {
      const rect = element.nativeElement.getBoundingClientRect();
      return rect.bottom >= 0 && rect.top <= windowHeight;
    });
    if (firstVisibleElement) {
      this.updateCurrentChapter(firstVisibleElement.nativeElement.id);
    }

    const firstVisibleImage = this.imageElements!.find((element) => {
      const rect = element.nativeElement.getBoundingClientRect();
      return rect.bottom >= 0 && rect.top <= windowHeight;
    });
    if (firstVisibleImage) {
      this.updateCurrentImage(
        +firstVisibleImage.nativeElement.getAttribute('data-index'),
      );
    }
  }

  handleRange(event: any): void {
    const index = event.detail.value;
    const image = document.getElementById(
      `${this.currentChapter!.id}/${index}`,
    );
    if (image) {
      const y = (image.offsetParent as HTMLElement).offsetTop;
      this.content?.scrollToPoint(0, y, 1500);
    }
  }

  handleBottomReached(event: any): void {
    let nextChapter = this.chapters[this.chapters.length - 1].next;
    if (nextChapter) {
      this.loadChapter(nextChapter)
        .then((chapterData: ChapterData) => {
          this.chapters.push(chapterData);
        })
        .catch(async (err) => {
          await this.toastService.logError(err);
        })
        .finally(() => event.target.complete());
    } else {
      event.target.complete();
    }
  }

  handleTopReached(event: any): void {
    let previousChapter = this.chapters[0].previous;
    if (previousChapter) {
      this.loadChapter(previousChapter)
        .then((chapterData: ChapterData) => {
          this.chapters.unshift(chapterData);
          this.updateCurrentChapter(previousChapter!);
        })
        .catch(async (err) => {
          await this.toastService.logError(err);
        })
        .finally(() => event.target.complete());
    } else {
      event.target.complete();
    }
  }

  updateCurrentChapter(chapterId: string): void {
    const index = this.chapters.findIndex((c) => c.id === chapterId);
    if (index === -1) {
      return;
    }

    this.currentChapter = this.chapters[index];

    this.chapters.splice(0, index - 1);
    this.chapters.splice(index + 2, this.chapters.length - index - 2);
  }

  updateCurrentImage(index: number): void {
    this.currentImageIndex = index;
  }

  findPageIndex(): number {
    const pageIndex: string = this.utils.getParam(this.route, 'pageIndex', '0');
    let pageNumber: number = parseInt(pageIndex);
    if (pageNumber < 0 || pageNumber >= this.manga!.chapters.length) {
      throw Error('Invalid page index');
    }
    return pageNumber;
  }

  async fetchManga(): Promise<Manga> {
    const connector: string = this.utils.getParam(this.route, 'connector');
    const mangaId: string = this.utils.getParam(this.route, 'mangaId');
    if (!connector || !mangaId) {
      throw Error('Invalid connector / manga id');
    }

    return await firstValueFrom(this.apiService.fetchManga(connector, mangaId));
  }

  async loadChapter(chapterId: string): Promise<ChapterData> {
    const chapter: Chapter = await firstValueFrom(
      this.apiService.fetchChapter(this.manga!.connector, chapterId),
    );
    let index: number = this.manga!.chapters.findIndex(
      (c) => c.id === chapterId,
    );
    let title = this.manga!.chapters[index].title;
    let next: string | null =
      index > 0 ? this.manga!.chapters[index - 1].id : null;
    let previous: string | null =
      index + 1 < this.manga!.chapters.length
        ? this.manga!.chapters[index + 1].id
        : null;

    return {
      images: chapter.images.map((image) =>
        this.apiService.getImageLink(this.manga!.connector, image),
      ),
      title,
      id: chapterId,
      previous,
      next,
    };
  }
}
