import { Component, OnInit } from '@angular/core';
import { YomugamiApiService } from '../../services/yomugami-api.service';
import { MangaPreview } from '../../interfaces/manga';
import { firstValueFrom } from 'rxjs';
import { NotificationToastService } from '../../services/notification-toast.service';
import { StorageService } from '../../services/storage.service';
import { SplashService } from '../../services/splash.service';

@Component({
  selector: 'app-explore',
  templateUrl: 'explore.page.html',
  styleUrls: ['explore.page.scss'],
})
export class ExplorePage implements OnInit {
  isLoadingConnectors: boolean = true;
  isLoadingMangas: boolean = false;
  isError: boolean = false;
  connectors: string[] = [];
  searchInput: string = '';
  mangaList: MangaPreview[] = [];

  constructor(
    private apiService: YomugamiApiService,
    private toastService: NotificationToastService,
    private splashService: SplashService,
    public storageService: StorageService,
  ) {}

  async ngOnInit(): Promise<void> {
    await this.fetchConnectors();
    this.splashService.hideSplash();
  }

  handleRefresh(event: any): void {
    if (this.mangaList.length === 0) {
      this.fetchConnectors(true).then(() => event.target.complete());
    } else {
      this.searchManga(true).then(() => event.target.complete());
    }
  }

  handleBookmarkClicked(event: MouseEvent, manga: MangaPreview): void {
    event.stopPropagation();
    event.preventDefault();
    void this.storageService.toggleSavedManga({
      connector: manga.connector,
      id: manga.id,
    });
  }

  getImageCover(manga: MangaPreview): string {
    return this.apiService.getImageLink(manga.connector, manga.cover);
  }

  getMangaLink(manga: MangaPreview): string {
    return this.apiService.makeUrl('', manga.connector, 'manga', manga.id);
  }

  async fetchConnectors(reload: boolean = false): Promise<void> {
    this.isLoadingConnectors = true;
    this.isError = false;
    try {
      this.connectors = await firstValueFrom(
        this.apiService.fetchConnectors(reload),
      );
    } catch (err) {
      this.isError = true;
      await this.toastService.logError(err);
    }
    this.isLoadingConnectors = false;
  }

  async searchManga(reload: boolean = false): Promise<void> {
    if (this.searchInput !== '') {
      this.mangaList = [];
      this.isLoadingMangas = true;
      for await (const conn of this.connectors) {
        try {
          let mangas: MangaPreview[] = await firstValueFrom(
            this.apiService.searchManga(conn, this.searchInput, reload),
          );
          this.mangaList.push(...mangas);
        } catch (err) {
          await this.toastService.logError(err);
        }
      }
      this.isLoadingMangas = false;
    }
  }
}
