import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MangaPageRoutingModule } from './manga-routing.module';

import { MangaPage } from './manga.page';
import { ChaptersModalModule } from '../../components/chapters-modal/chapters-modal.module';
import {ImageModule} from '../../components/image/image.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MangaPageRoutingModule,
    ChaptersModalModule,
    ImageModule,
  ],
  declarations: [MangaPage],
})
export class MangaPageModule {}
