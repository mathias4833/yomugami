import { Component, OnInit, ViewChild } from '@angular/core';
import { Manga } from '../../interfaces/manga';
import { ActivatedRoute } from '@angular/router';
import { YomugamiApiService } from '../../services/yomugami-api.service';
import { firstValueFrom } from 'rxjs';
import { NotificationToastService } from '../../services/notification-toast.service';
import { UtilsService } from '../../services/utils.service';
import { ViewWillEnter, ViewWillLeave } from '@ionic/angular';
import { ChaptersModalComponent } from '../../components/chapters-modal/chapters-modal.component';
import { StorageService } from '../../services/storage.service';
import { SplashService } from '../../services/splash.service';

@Component({
  selector: 'app-manga',
  templateUrl: './manga.page.html',
  styleUrls: ['./manga.page.scss'],
})
export class MangaPage implements OnInit, ViewWillEnter, ViewWillLeave {
  @ViewChild(ChaptersModalComponent) chapterModal:
    | ChaptersModalComponent
    | undefined;

  isLoading: boolean = true;
  isError: boolean = false;
  manga: Manga | undefined;

  constructor(
    private utils: UtilsService,
    private route: ActivatedRoute,
    private apiService: YomugamiApiService,
    private toastService: NotificationToastService,
    private splashService: SplashService,
    public storageService: StorageService,
  ) {}

  async ngOnInit(): Promise<void> {
    await this.fetchManga();
    this.splashService.hideSplash();
  }

  ionViewWillEnter() {
    if (!this.isLoading && !this.isError) {
      this.chapterModal?.openModal();
    }
  }

  ionViewWillLeave() {
    this.chapterModal?.closeModal();
  }

  handleRefresh(event: any): void {
    this.fetchManga(true).then(() => event.target.complete());
  }

  getImageCover(): string {
    return this.apiService.getImageLink(
      this.manga!.connector,
      this.manga!.cover,
    );
  }

  async fetchManga(reload: boolean = false): Promise<void> {
    this.isLoading = true;
    this.isError = false;

    const connector: string = this.utils.getParam(this.route, 'connector');
    const mangaId: string = this.utils.getParam(this.route, 'mangaId');
    if (!connector || !mangaId) {
      this.isLoading = false;
      this.isError = true;
      return;
    }

    try {
      this.manga = await firstValueFrom(
        this.apiService.fetchManga(connector, mangaId, reload),
      );
      this.chapterModal?.openModal();
    } catch (err) {
      this.isError = true;
      await this.toastService.logError(err);
    }
    this.isLoading = false;
  }
}
