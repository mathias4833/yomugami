import { Component, OnInit } from '@angular/core';
import { SplashService } from '../../services/splash.service';

@Component({
  selector: 'app-history',
  templateUrl: 'history.page.html',
  styleUrls: ['history.page.scss'],
})
export class HistoryPage implements OnInit {
  constructor(private splashService: SplashService) {}

  ngOnInit() {
    this.splashService.hideSplash();
  }
}
