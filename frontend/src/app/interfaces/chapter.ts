export interface ChapterPreview {
  id: string;
  title: string;
  date: Date;
}

export interface Chapter {
  images: string[];
}

export interface ChapterData {
  images: string[];
  id: string;
  title: string;
  previous: string | null;
  next: string | null;
}
