import { ChapterPreview } from './chapter';

export interface SearchMangaResponse {
  mangas: MangaPreview[];
}

export interface MangaPreview {
  connector: string;
  id: string;
  title: string;
  cover: string;
}

export interface Manga {
  connector: string;
  id: string;
  title: string;
  cover: string;
  description: string;
  year: number;
  status: string;
  chapters: ChapterPreview[];
}

export interface SavedManga {
  connector: string;
  id: string;
}
