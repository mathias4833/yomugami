import { Component, OnInit } from '@angular/core';
import { SplashService } from '../../services/splash.service';
import { StorageService } from '../../services/storage.service';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.component.html',
  styleUrls: ['./splash.component.scss'],
})
export class SplashComponent implements OnInit {
  isStorageInitialized: boolean = false;
  showSplash: boolean = true;

  constructor(
    private splashService: SplashService,
    private storageService: StorageService,
  ) {}

  ngOnInit() {
    this.storageService.init().then(() => {
      this.isStorageInitialized = true;
    });
    this.splashService.visibility.subscribe((isVisible) => {
      this.showSplash = isVisible;
    });
  }
}
