import { ChangeDetectorRef, Component, Input } from '@angular/core';
import { ChapterPreview } from '../../interfaces/chapter';
import { YomugamiApiService } from '../../services/yomugami-api.service';
import { Manga } from '../../interfaces/manga';

@Component({
  selector: 'app-chapters-modal',
  templateUrl: './chapters-modal.component.html',
  styleUrls: ['./chapters-modal.component.scss'],
})
export class ChaptersModalComponent {
  modalSize: number = 40;
  isOpen: boolean = false;

  @Input()
  manga!: Manga;

  constructor(
    private apiService: YomugamiApiService,
    private changeDetectorRef: ChangeDetectorRef,
  ) {}

  openModal() {
    this.isOpen = true;
  }

  closeModal() {
    this.isOpen = false;
    this.changeDetectorRef.detectChanges();
  }

  handleResize(event: any): void {
    this.modalSize = event.detail.breakpoint * 100;
  }

  getReaderLink(chapter: ChapterPreview): string {
    return this.apiService.makeUrl(
      '',
      this.manga.connector,
      'reader',
      this.manga.id,
      chapter.id,
      '0',
    );
  }
}
