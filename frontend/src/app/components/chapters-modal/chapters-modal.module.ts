import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { ChaptersModalComponent } from './chapters-modal.component';
import { RouterLink } from '@angular/router';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, RouterLink],
  declarations: [ChaptersModalComponent],
  exports: [ChaptersModalComponent],
})
export class ChaptersModalModule {}
