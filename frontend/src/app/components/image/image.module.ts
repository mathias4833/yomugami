import { NgModule } from '@angular/core';
import {CommonModule, NgOptimizedImage} from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { RouterLink } from '@angular/router';
import {ImageComponent} from './image.component';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, RouterLink, NgOptimizedImage],
  declarations: [ImageComponent],
  exports: [ImageComponent],
})
export class ImageModule {}
