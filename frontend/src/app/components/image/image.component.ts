import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss'],
})
export class ImageComponent {
  @Input()
  alt: string = '';
  @Input()
  src: string = '';
  @Input()
  allowRetry: boolean = false;
  didLoad: boolean = false;
  didFail: boolean = false;


  constructor() { }

  protected readonly console = console;
}
