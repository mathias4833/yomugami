import {TestBed} from '@angular/core/testing';

import {YomugamiApiService} from './yomugami-api.service';

describe('YomugamiApiService', () => {
  let service: YomugamiApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(YomugamiApiService);
  });
  
  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
