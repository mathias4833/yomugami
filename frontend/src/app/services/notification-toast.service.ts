import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class NotificationToastService {
  constructor(private toastController: ToastController) {}

  private getErrorMessage(error: unknown): string {
    if (typeof error === 'string') {
      return error;
    } else if (error instanceof Error) {
      return error.message;
    } else if (
      error instanceof Object &&
      (error as HttpErrorResponse).name === 'HttpErrorResponse'
    ) {
      return (error as HttpErrorResponse).message;
    } else {
      return String(error);
    }
  }

  async logError(error: unknown): Promise<void> {
    console.log(error);
    const toast = await this.toastController.create({
      message: this.getErrorMessage(error),
      duration: 3000,
      position: 'bottom',
    });
    await toast.present();
  }
}
