import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { SavedManga } from '../interfaces/manga';

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  private _storage: Storage | null = null;
  private savedMangas: Set<string> = new Set();

  constructor(private storage: Storage) {}

  async init(): Promise<void> {
    this._storage = await this.storage.create();
    await this.readSavedMangas();
  }

  private async readSavedMangas(): Promise<void> {
    if (this._storage) {
      const data: string = await this._storage.get('savedMangas');
      this.savedMangas = new Set(JSON.parse(data));
    }
  }

  private async writeSavedMangas(): Promise<void> {
    const data: string = JSON.stringify([...this.savedMangas]);
    await this._storage?.set('savedMangas', data);
  }

  private savedMangaToString(savedManga: SavedManga): string {
    return JSON.stringify([savedManga.connector, savedManga.id]);
  }

  private stringToSavedManga(data: string): SavedManga {
    const [connector, id] = JSON.parse(data);
    return { connector, id };
  }

  getSavedMangas(): SavedManga[] {
    return [...this.savedMangas].map(this.stringToSavedManga);
  }

  isSaved(savedManga: SavedManga): boolean {
    const data: string = this.savedMangaToString(savedManga);
    return this.savedMangas.has(data);
  }

  async addSavedManga(savedManga: SavedManga): Promise<void> {
    const data: string = this.savedMangaToString(savedManga);
    this.savedMangas.add(data);
    await this.writeSavedMangas();
  }

  async removeSavedManga(savedManga: SavedManga): Promise<void> {
    const data: string = this.savedMangaToString(savedManga);
    this.savedMangas.delete(data);
    await this.writeSavedMangas();
  }

  async toggleSavedManga(savedManga: SavedManga): Promise<void> {
    if (this.isSaved(savedManga)) {
      await this.removeSavedManga(savedManga);
    } else {
      await this.addSavedManga(savedManga);
    }
  }
}
