import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SplashService {
  private visibilitySubject: Subject<boolean> = new Subject<boolean>();
  private isHidden = false;

  get visibility(): Observable<boolean> {
    if (this.isHidden) {
      return new Observable();
    }
    return this.visibilitySubject.asObservable();
  }

  hideSplash() {
    if (!this.isHidden) {
      this.visibilitySubject.next(false);
      this.isHidden = true;
      this.visibilitySubject.complete();
    }
  }
}
