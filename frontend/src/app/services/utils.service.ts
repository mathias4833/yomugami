import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class UtilsService {
  constructor() {}

  getParam(
    route: ActivatedRoute,
    name: string,
    defaultValue: string = '',
  ): string {
    let param = route.snapshot.paramMap.get(name);
    if (param !== null) {
      return decodeURIComponent(param);
    } else {
      return defaultValue;
    }
  }
}
