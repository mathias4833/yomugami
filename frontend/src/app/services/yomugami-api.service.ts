import { Injectable } from '@angular/core';
import { Manga, MangaPreview, SearchMangaResponse } from '../interfaces/manga';
import { map, Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Chapter, ChapterPreview } from '../interfaces/chapter';

@Injectable({
  providedIn: 'root',
})
export class YomugamiApiService {
  readonly apiUrl = 'http://localhost:8080/api';
  private readonly cacheReloadHeaders: HttpHeaders = new HttpHeaders().set(
    'Cache-Control',
    'reload',
  );

  constructor(private http: HttpClient) {}

  public makeUrl(base: string, ...segments: string[]): string {
    return segments.reduce(
      (acc: string, s: string) => `${acc}/${encodeURIComponent(s)}`,
      base,
    );
  }

  public fetchConnectors(reloadCache: boolean = false): Observable<string[]> {
    return this.http.get<string[]>(
      this.makeUrl(this.apiUrl, 'connectors'),
      reloadCache ? { headers: this.cacheReloadHeaders } : {},
    );
  }

  public searchManga(
    connector: string,
    title: string,
    reloadCache: boolean = false,
  ): Observable<MangaPreview[]> {
    return this.http
      .get<SearchMangaResponse>(
        this.makeUrl(this.apiUrl, connector, 'search', title),
        reloadCache ? { headers: this.cacheReloadHeaders } : {},
      )
      .pipe(
        map((data: SearchMangaResponse) =>
          data.mangas.map((m: MangaPreview) => {
            m.connector = connector;
            return m;
          }),
        ),
      );
  }

  public fetchManga(
    connector: string,
    mangaId: string,
    reloadCache: boolean = false,
  ): Observable<Manga> {
    return this.http
      .get<Manga>(
        this.makeUrl(this.apiUrl, connector, 'manga', mangaId),
        reloadCache ? { headers: this.cacheReloadHeaders } : {},
      )
      .pipe(
        map((m: Manga) => {
          m.connector = connector;
          m.chapters = m.chapters.map((c: ChapterPreview) => {
            c.date = new Date(c.date);
            return c;
          });
          return m;
        }),
      );
  }

  public fetchChapter(
    connector: string,
    chapterId: string,
    reloadCache: boolean = false,
  ): Observable<Chapter> {
    return this.http.get<Chapter>(
      this.makeUrl(this.apiUrl, connector, 'chapter', chapterId),
      reloadCache ? { headers: this.cacheReloadHeaders } : {},
    );
  }

  public getImageLink(connector: string, imageId: string): string {
    return this.makeUrl(this.apiUrl, connector, 'image', imageId);
  }
}
